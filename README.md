# ansible-archlinux

A set of [Ansible][url-ansible] playbooks to set up my personal computer(s)
running [Arch Linux][url-archlinux], just the way I like them.

## Requirements

`ansible` needs to be installed.

## Usage

```shell
ansible-playbook playbook.yml
```

## License

The Ansible [AUR][url-ansible-aur] module is licensed as GPLv3.
Everything else is [MIT](LICENSE).

[url-ansible]: https://ansible.com
[url-archlinux]: https://archlinux.org
[url-ansible-aur]: https://github.com/kewlfft/ansible-aur
